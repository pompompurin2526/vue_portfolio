import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        background: '#D3E173',
      },
      dark: {
        background: '#F3E5F5',
      },
    },
  },
});
