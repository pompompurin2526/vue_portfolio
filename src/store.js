import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

//state グローバル変数みたいなもの
export default new Vuex.Store({
  state: {
    count: 2,
  },

  //gettersに記載することで全てで利用することができる！
  getters: {
    doubleCount: (state) =>
      state.count * 2,
  },
});
