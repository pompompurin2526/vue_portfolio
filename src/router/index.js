import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home.vue';
import Profile from '@/components/Profile.vue';
import History from '@/components/History.vue';
import Skills from '@/components/Skills.vue';
import Form from '@/components/Form.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
    },
    {
      path: '/history',
      name: 'History',
      component: History,
    },
    {
      path: '/skills',
      name: 'Skills',
      component: Skills,
    },
    {
      path: '/form',
      name: 'Form',
      component: Form,
    },
  ],
});
