import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import router from './router';
import axios from 'axios';
import store from './store';

Vue.config.productionTip = false;

axios.defaults.baseURL =
  'https://firestore.googleapis.com/v1/projects/vuejs-http-ad54c/databases/(default)/documents';

new Vue({
  vuetify,
  router,
  store,
  // ここに記載すると、この以下のVueインスタンスに全てアクセスすることができる
  render: (h) => h(App),
}).$mount('#app');
